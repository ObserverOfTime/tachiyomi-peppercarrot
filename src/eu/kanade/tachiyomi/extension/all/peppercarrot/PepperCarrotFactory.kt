package eu.kanade.tachiyomi.extension.all.peppercarrot

import eu.kanade.tachiyomi.source.SourceFactory
import java.security.MessageDigest

// https://www.peppercarrot.com/0_sources/langs.json
class PepperCarrotFactory : SourceFactory {
    override fun createSources() =
        listOf(
            PepperCarrotEN(),
            PepperCarrotFR(),
            PepperCarrotDE(),
            PepperCarrotES(),
            PepperCarrotPL(),
            PepperCarrotRU(),
            PepperCarrotCN(),
            PepperCarrotJA(),
            PepperCarrotMX(),
            PepperCarrotEO(),
            PepperCarrotVI(),
            PepperCarrotCA(),
            PepperCarrotID(),
            PepperCarrotIT(),
            PepperCarrotKW(),
            PepperCarrotNL(),
            PepperCarrotNN(),
            PepperCarrotRO(),
            PepperCarrotSL(),
            PepperCarrotAR(),
            PepperCarrotAT(),
            PepperCarrotBN(),
            PepperCarrotBR(),
            PepperCarrotCS(),
            PepperCarrotDA(),
            PepperCarrotEL(),
            PepperCarrotFA(),
            PepperCarrotFI(),
            PepperCarrotGA(),
            PepperCarrotGD(),
            PepperCarrotHE(),
            PepperCarrotHI(),
            PepperCarrotHU(),
            PepperCarrotIE(),
            PepperCarrotIO(),
            PepperCarrotJB(),
            PepperCarrotJZ(),
            PepperCarrotKH(),
            PepperCarrotKR(),
            PepperCarrotKT(),
            PepperCarrotLA(),
            PepperCarrotLF(),
            PepperCarrotLS(),
            PepperCarrotLT(),
            PepperCarrotML(),
            PepperCarrotMS(),
            PepperCarrotMW(),
            PepperCarrotNO(),
            PepperCarrotNS(),
            PepperCarrotOC(),
            PepperCarrotPH(),
            PepperCarrotPT(),
            PepperCarrotSI(),
            PepperCarrotSK(),
            PepperCarrotSR(),
            PepperCarrotSU(),
            PepperCarrotSV(),
            PepperCarrotSZ(),
            PepperCarrotTR(),
            PepperCarrotUK(),
            PepperCarrotGB(),
            PepperCarrotGO(),
            PepperCarrotRC(),
            PepperCarrotLD(),
            PepperCarrotNM(),
            PepperCarrotSB(),
            PepperCarrotSP(),
            PepperCarrotTP(),
        )
}

// frontpage languages

class PepperCarrotEN : PepperCarrot("en")

class PepperCarrotFR : PepperCarrot("fr")

class PepperCarrotDE : PepperCarrot("de")

class PepperCarrotES : PepperCarrot("es")

class PepperCarrotPL : PepperCarrot("pl")

class PepperCarrotRU : PepperCarrot("ru")

class PepperCarrotCN : PepperCarrot("zh-Hans-CN", "cn") {
    override val leftParen = '（'
}

class PepperCarrotJA : PepperCarrot("ja") {
    override val leftParen = '（'
}

class PepperCarrotMX : PepperCarrot("es-MX", "mx")

class PepperCarrotEO : PepperCarrot("eo")

class PepperCarrotVI : PepperCarrot("vi")

// 100% completed languages

class PepperCarrotCA : PepperCarrot("ca")

class PepperCarrotID : PepperCarrot("id")

class PepperCarrotIT : PepperCarrot("it")

class PepperCarrotKW : PepperCarrot("kw")

class PepperCarrotNL : PepperCarrot("nl")

class PepperCarrotNN : PepperCarrot("nn")

class PepperCarrotRO : PepperCarrot("ro")

class PepperCarrotSL : PepperCarrot("sl")

// other languages

class PepperCarrotAR : PepperCarrot("ar")

class PepperCarrotAT : PepperCarrot("ast", "at")

class PepperCarrotBN : PepperCarrot("bn")

class PepperCarrotBR : PepperCarrot("br")

class PepperCarrotCS : PepperCarrot("cs")

class PepperCarrotDA : PepperCarrot("da")

class PepperCarrotEL : PepperCarrot("el")

class PepperCarrotFA : PepperCarrot("fa")

class PepperCarrotFI : PepperCarrot("fi")

class PepperCarrotGA : PepperCarrot("oc-gascon", "ga")

class PepperCarrotGD : PepperCarrot("gd")

class PepperCarrotHE : PepperCarrot("he-IL", "he")

class PepperCarrotHI : PepperCarrot("hi")

class PepperCarrotHU : PepperCarrot("hu")

class PepperCarrotIE : PepperCarrot("ie")

class PepperCarrotIO : PepperCarrot("io")

class PepperCarrotJB : PepperCarrot("jbo-Latn", "jb")

class PepperCarrotJZ : PepperCarrot("jbo", "jz")

class PepperCarrotKH : PepperCarrot("ko-Hang", "kh")

class PepperCarrotKR : PepperCarrot("ko-Kore", "kr")

class PepperCarrotKT : PepperCarrot("avk", "kt")

class PepperCarrotLA : PepperCarrot("la")

class PepperCarrotLF : PepperCarrot("lfn", "lf")

class PepperCarrotLS : PepperCarrot("es-419", "ls")

class PepperCarrotLT : PepperCarrot("lt")

class PepperCarrotML : PepperCarrot("ml")

class PepperCarrotMS : PepperCarrot("ms")

class PepperCarrotMW : PepperCarrot("mwl", "mw")

class PepperCarrotNO : PepperCarrot("nb", "no")

class PepperCarrotNS : PepperCarrot("nds", "ns")

class PepperCarrotOC : PepperCarrot("oc")

class PepperCarrotPH : PepperCarrot("fil", "ph")

class PepperCarrotPT : PepperCarrot("pt-BR", "pt")

class PepperCarrotSI : PepperCarrot("si")

class PepperCarrotSK : PepperCarrot("sk")

class PepperCarrotSR : PepperCarrot("sr-Cyrl", "sr")

class PepperCarrotSU : PepperCarrot("su")

class PepperCarrotSV : PepperCarrot("sv")

class PepperCarrotSZ : PepperCarrot("szl", "sz")

class PepperCarrotTR : PepperCarrot("tr")

class PepperCarrotUK : PepperCarrot("uk")

// languages without standard language codes

// gb and glb have been proposed but not yet added to ISO 639
class PepperCarrotGB : PepperCarrot("Globasa", "gb")

// fr-gallo is in IETF BCP 47 but not in ISO 639
class PepperCarrotGO : PepperCarrot("Gallo", "go")

// ldn is in ISO 693-3 but not recognised by java.util.Locale
// should be "Láadan" but java.util.Locale doesn't like that
class PepperCarrotLD : PepperCarrot("Laadan", "ld") {
    override val id = generateId(lang = "ldn")
}

// nrf is in ISO 693-3 but not recognised by java.util.Locale
class PepperCarrotNM : PepperCarrot("Normaund", "nm") {
    override val id = generateId(lang = "nrf")
}

// rcf is in ISO 693-3 but not recognised by java.util.Locale
// should be "Kréol Rényoné" but java.util.Locale doesn't like that
class PepperCarrotRC : PepperCarrot("Kreol-Renyone", "rc") {
    override val id = generateId(lang = "rcf")
}

// sph has been requested but not yet added to ISO 639
class PepperCarrotSB : PepperCarrot("Sambahsa", "sb")

// sitelen pona doesn't have its own ISO 15924 script code
// should be "Sitelen Pona" but java.util.Locale doesn't like that
class PepperCarrotSP : PepperCarrot("Pona-Sitelen", "sp") {
    override val id = generateId(lang = "tok-Zsym")
}

// tok is in ISO 693-3 but not recognised by java.util.Locale
// should be "Toki Pona" but java.util.Locale doesn't like that
class PepperCarrotTP : PepperCarrot("Pona-Toki", "tp") {
    override val id = generateId(lang = "tok")
}

// languages without chapter translations

// class PepperCarrotGL : PepperCarrot("gl")

// https://github.com/mihonapp/mihon/blob/main/source-api/src/commonMain/kotlin/eu/kanade/tachiyomi/source/online/HttpSource.kt#L87-L91
private fun PepperCarrot.generateId(lang: String): Long {
    val key = "${name.lowercase()}/$lang/$versionId"
    val bytes = MessageDigest.getInstance("MD5").digest(key.toByteArray())
    return (0..7).map { bytes[it].toLong() and 0xff shl 8 * (7 - it) }.reduce(Long::or) and Long.MAX_VALUE
}
