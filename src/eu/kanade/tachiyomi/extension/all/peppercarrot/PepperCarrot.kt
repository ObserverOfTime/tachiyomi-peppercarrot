package eu.kanade.tachiyomi.extension.all.peppercarrot

import android.app.Application
import androidx.preference.PreferenceScreen
import androidx.preference.SwitchPreferenceCompat
import eu.kanade.tachiyomi.network.GET
import eu.kanade.tachiyomi.network.asObservableSuccess
import eu.kanade.tachiyomi.source.ConfigurableSource
import eu.kanade.tachiyomi.source.model.FilterList
import eu.kanade.tachiyomi.source.model.MangasPage
import eu.kanade.tachiyomi.source.model.Page
import eu.kanade.tachiyomi.source.model.SChapter
import eu.kanade.tachiyomi.source.model.SManga
import eu.kanade.tachiyomi.source.online.HttpSource
import eu.kanade.tachiyomi.util.asJsoup
import okhttp3.Response
import org.jsoup.select.Evaluator
import rx.Observable
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.text.SimpleDateFormat
import java.util.Locale

abstract class PepperCarrot(
    final override val lang: String,
    siteLang: String = lang,
) : ConfigurableSource, HttpSource() {
    final override val supportsLatest = false

    final override val name = "Pepper&Carrot"

    final override val baseUrl = "https://www.peppercarrot.com/$siteLang/"

    protected open val leftParen = '('

    private val preferences by lazy {
        Injekt.get<Application>().getSharedPreferences("source_$id", 0x0000)!!
    }

    private val hires: Boolean
        get() = preferences.getBoolean(HIRES_KEY, false)

    // strip /$siteLang/ from the chapter URL
    override fun pageListRequest(chapter: SChapter) = GET(baseUrl + chapter.url.substring(4), headers)

    override fun chapterListParse(response: Response) =
        response.asJsoup().body().select(chapterListEvaluator).map {
            SChapter.create().apply {
                val anchor = it.child(0)
                val caption = it.child(1).ownText()
                setUrlWithoutDomain(anchor.attr("href"))
                name = anchor.child(0).attr("title").substringBeforeLast(leftParen).trimEnd()
                chapter_number = url.substringAfter("/ep").substringBefore("_").toFloat()
                date_upload = dateRegex.find(caption)?.value?.run(dateFormat::parse)?.time ?: 0L
            }
        }

    override fun pageListParse(response: Response) =
        response.asJsoup().body().select(pageListEvaluator).mapIndexed { idx, img ->
            Page(idx, "", img.attr("src").let { if (!hires) it else it.replace("low-res", HIRES_KEY) })
        }

    override fun fetchPopularManga(page: Int): Observable<MangasPage> =
        client.newCall(GET(baseUrl, headers)).asObservableSuccess().map {
            val details = it.asJsoup().selectFirst(mangaDetailsEvaluator)
            val manga = SManga.create().apply {
                url = COMIC_URL
                author = CREATOR
                artist = CREATOR
                status = SManga.ONGOING
                thumbnail_url = COVER_URL
                title = details?.child(1)?.attr("title") ?: "Untitled"
                description = details?.child(2)?.text()
            }
            MangasPage(listOf(manga), false)
        }

    override fun fetchSearchManga(page: Int, query: String, filters: FilterList) = fetchPopularManga(page)

    override fun fetchMangaDetails(manga: SManga) = Observable.just(manga.apply { initialized = true })!!

    override fun setupPreferenceScreen(screen: PreferenceScreen) {
        SwitchPreferenceCompat(screen.context).apply {
            key = HIRES_KEY
            title = "High resolution"
            setDefaultValue(false)
            setOnPreferenceChangeListener { _, newValue ->
                preferences.edit().putBoolean(HIRES_KEY, newValue as Boolean).commit()
            }
        }.let(screen::addPreference)
    }

    override fun latestUpdatesRequest(page: Int) = throw UnsupportedOperationException()

    override fun popularMangaRequest(page: Int) = throw UnsupportedOperationException()

    override fun searchMangaRequest(page: Int, query: String, filters: FilterList) =
        throw UnsupportedOperationException()

    override fun latestUpdatesParse(response: Response) = throw UnsupportedOperationException()

    override fun popularMangaParse(response: Response) = throw UnsupportedOperationException()

    override fun searchMangaParse(response: Response) = throw UnsupportedOperationException()

    override fun mangaDetailsParse(response: Response) = throw UnsupportedOperationException()

    override fun imageUrlParse(response: Response) = throw UnsupportedOperationException()

    companion object {
        private const val CREATOR = "David Revoy"

        private const val COMIC_URL = "webcomics/index.html"

        private const val COVER_URL =
            "https://www.peppercarrot.com/0_sources/0ther/archive/First-cover.png"

        private const val HIRES_KEY = "hi-res"

        private val dateRegex = Regex("""\d{4}-\d{2}-\d{2}""")

        private val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ROOT)

        private val mangaDetailsEvaluator = Evaluator.Class("covertextoverlay")

        private val chapterListEvaluator = Evaluator.Class("translated")

        private val pageListEvaluator = Evaluator.Class("comicpage")
    }
}
