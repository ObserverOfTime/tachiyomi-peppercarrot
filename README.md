# Pepper&Carrot Tachiyomi Extension

This repository contains a [Pepper&Carrot](https://www.peppercarrot.com/) extension for the Tachiyomi app and its derivatives.

## Usage

Extension sources can be downloaded, installed, and uninstalled via the main Tachiyomi app.
<br/>They are installed and uninstalled like regular apps, in `.apk` format.

### Add the Repository

<object data="https://img.shields.io/badge/One%20Click%20Install-818cf8?style=for-the-badge&link=tachiyomi%3A%2F%2Fadd-repo%3Furl%3Dhttps%3A%2F%2Fobserveroftime.frama.io%2Ftachiyomi-peppercarrot%2Findex.min.json" type="image/svg+xml">
  <a href="tachiyomi://add-repo?url=https://observeroftime.frama.io/tachiyomi-peppercarrot/index.min.json">
    <img src="https://img.shields.io/badge/One%20Click%20Install-818cf8?style=for-the-badge&link=tachiyomi%3A%2F%2Fadd-repo%3Furl%3Dhttps%3A%2F%2Fobserveroftime.frama.io%2Ftachiyomi-peppercarrot%2Findex.min.json" alt="One Click Install">
  </a>
</object>

If the button doesn't work, add the repo manually using this link:
<br/>https://observeroftime.frama.io/tachiyomi-peppercarrot/index.min.json

### Downloads

Alternatively, you can download the `.apk` from [here](https://framagit.org/ObserverOfTime/tachiyomi-peppercarrot/-/releases/permalink/latest) and install it manually.
